If you want to learn more about Machine Learning, we can recommend the following online-available book:

Gareth James, Daniela Witten, Trevor Hastie and Robert Tibshirani
An Introduction to Statistical Learning with Applications in R

http://faculty.marshall.usc.edu/gareth-james/ISL/