{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tomato sweetness estimation based on multi-spectral imaging\n",
    "In this project, you will investigate different machine-learning algorithms in order to estimate the sweetness of tomatoes based on the reflectance spectrum measured using a hyper-spectral camera.\n",
    "\n",
    "With a refractometer, see Figure below, one can measure the index of refraction of a solution. If the solution is taken from a fruit, the index can be calibrated to measure the so-called Degree Brix (°Brix), which is a measure of the soluble-solids content. For tomatoes, these soluble solids are mainly the sugars. In other words, °Brix is a measure of the amount of sugar in a solution.\n",
    "<img src=\"figures/refractometer.png\" width=300>\n",
    "\n",
    "Spectrophotometry has been used to estimate soluble solids non-destructively. In (Slaughter et al. 1996), for instance, a spectrophotometer was used that emits monochromatic light at different wavelengths onto the tomato and measures the absorption of light at these wavelengths using a light detector. This results in an absorption spectrum. Looking into the visual and near-infrared spectrum (300-1100nm) and applying Partial Least Squares Regression (PLS-R), the authors were able to predict the sugar content of tomatoes with a standard error of calibration of 0.27 °Brix.\n",
    "\n",
    "The use of spectrophotometry provides accurate estimations, but has the big disadvantage that the tomato needs to be brought to the sensor, which makes it impractical for large samples. Hyperspectral imaging provides a solution for this. A hyperspectral camera creates an image of the scene, where each pixel contains the full spectrum. The image below illustrates the spectral image, which is a 3D data structure with 2 spatial axes, x and y (ignore ‘time’ in the image) and a spectral axis, l. You can see that each pixel contains the reflection spectrum, illustrated by the wavelength vs reflection plot.\n",
    "\n",
    "<img src=\"figures/reflection.png\" width=600>\n",
    "\n",
    "To understand a spectrum, these are some reference wavelengths:\n",
    "* Blue 450-490 nm\n",
    "* Green 520-560 nm\n",
    "* Red 635-700 nm\n",
    "* Over 700 nm near infrared (not visible to humans)\n",
    "\n",
    "\n",
    "\n",
    "In this project, we use data from a lab experiment. In the experiment, three different tomato varieties have been used, variety A is a sweet cocktail tomato, variety B is middle-sweet tomato, and variety C is a large tomato with low sweetness. The image below provides RGB images of the varieties.\n",
    "\n",
    "<img src=\"figures/tomato_varieties.png\" width=600>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We first load the libraries again that we need to train the regression methods and to show the results.\n",
    "\n",
    "Run the code below:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import sklearn # Scikit-learn library which contains a lot of Machine Learning Models ready to use\n",
    "import pandas as pd # Library for data analysis. We will use it to load datasets\n",
    "import numpy as np # Library for highly efficient computations\n",
    "import matplotlib.pyplot as plt  # To create plots\n",
    "from sklearn.metrics import mean_squared_error, r2_score # Import some performance metrics\n",
    "from sklearn.model_selection import train_test_split # Split X and y in train and test sets\n",
    "from sklearn.preprocessing import PolynomialFeatures\n",
    "from sklearn.linear_model import LinearRegression # Import Linear Regression model from Scikit-Learn\n",
    "\n",
    "# Other imports and settings for better visualization of graphs\n",
    "from IPython import display\n",
    "import warnings\n",
    "warnings.filterwarnings(\"ignore\")\n",
    "%matplotlib inline\n",
    "plt.rcParams['figure.figsize'] = [10,8]\n",
    "plt.rcParams.update({'font.size': 12})\n",
    "\n",
    "# Import everything from utils.py\n",
    "from utils import *"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Loading the data\n",
    "\n",
    "Run the code below to load and view the data: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load dataset\n",
    "dataset = pd.read_csv('datasets/spectra_lab_ABC.csv')\n",
    "dataset.describe(include='all')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run the code below to look at some of the data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Answer\n",
    "## Write here your code(1 line)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run the code below to split the data in the three different varieties:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create sub-datasets per variety\n",
    "A = dataset[(dataset.variety=='A')].drop('variety', axis=1)\n",
    "B = dataset[(dataset.variety=='B')].drop('variety', axis=1)\n",
    "C = dataset[(dataset.variety=='C')].drop('variety', axis=1)\n",
    "\n",
    "dataset = dataset.drop('variety', axis=1)\n",
    "\n",
    "print('Nr tomatoes of variety A: ', A.shape[0])\n",
    "print('Nr tomatoes of variety B: ', B.shape[0])\n",
    "print('Nr tomatoes of variety C: ', C.shape[0])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The spectra per tomato\n",
    "\n",
    "Run the next cell to plot the spectrum of all the tomatoes for the varieties separate and for all combined:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get X and y from dataset using the given function\n",
    "X, y = get_X_y_data(dataset, y_name='brix', reduce_wavelengths=1)\n",
    "X_A, y_A = get_X_y_data(A, y_name='brix', reduce_wavelengths=1)\n",
    "X_B, y_B = get_X_y_data(B, y_name='brix', reduce_wavelengths=1)\n",
    "X_C, y_C = get_X_y_data(C, y_name='brix', reduce_wavelengths=1)\n",
    "\n",
    "X = X.values\n",
    "X_A = X_A.values\n",
    "X_B = X_B.values\n",
    "X_C = X_C.values\n",
    "n_columns = X.shape[1]\n",
    "step = (900 - 470)/n_columns\n",
    "x = [470+(i*step) for i in range(n_columns)]\n",
    "\n",
    "for i in range(len(X)):\n",
    "    plt.subplot(2,2,1) # Subplots grid: 2 row, 2 columns. This is subplot 1\n",
    "    plt.plot(x, X[i,:])\n",
    "    plt.title('All')\n",
    "    plt.xlabel('Wavelength [-]')\n",
    "    plt.ylabel('Reflection [-]')\n",
    "    \n",
    "for i in range(len(X_A)):\n",
    "    plt.subplot(2,2,2) # Subplots grid: 2 row, 2 columns. This is subplot 2\n",
    "    plt.plot(x, X_A[i,:])\n",
    "    plt.title('A')\n",
    "    plt.xlabel('Wavelength [-]')\n",
    "    plt.ylabel('Reflection [-]')\n",
    "    \n",
    "for i in range(len(X_B)):\n",
    "    plt.subplot(2,2,3) # Subplots grid: 2 row, 2 columns. This is subplot 3\n",
    "    plt.plot(x, X_B[i,:])\n",
    "    plt.title('B')\n",
    "    plt.xlabel('Wavelength [-]')\n",
    "    plt.ylabel('Reflection [-]')\n",
    "    \n",
    "for i in range(len(X_C)):\n",
    "    plt.subplot(2,2,4) # Subplots grid: 2 row, 2 columns. This is subplot 4\n",
    "    plt.plot(x, X_C[i,:])\n",
    "    plt.title('C')\n",
    "    plt.xlabel('Wavelength [-]')\n",
    "    plt.ylabel('Reflection [-]')\n",
    "    \n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Exercise 1:\n",
    "* Look at the spectra. Which wavelengths are most reflected by the tomatoes?\n",
    "* Can you identify some green tomatoes?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Sweetness (°Brix) of the tomatoes\n",
    "The °Brix of the tomatoes was measured destructively using a refractometer and can be considered as ground-truth measurements. Within and between varieties, there is variation in the °Brix, as indicated by the boxplots below.\n",
    "\n",
    "Run next cell to generate histograms of the °Brix values of the tomatoes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot histograms of brix values\n",
    "\n",
    "plt.subplot(2,2,1) # Subplots grid: 2 row, 2 columns. This is subplot 1\n",
    "plt.hist(dataset['brix'],10)\n",
    "plt.title('All')\n",
    "plt.xlabel('Brix')\n",
    "plt.ylabel('Frequency')\n",
    "\n",
    "plt.subplot(2,2,2) # Subplots grid: 2 row, 2 columns. This is subplot 2\n",
    "plt.hist(A['brix'],10)\n",
    "plt.title('A')\n",
    "plt.xlabel('Brix')\n",
    "plt.ylabel('Frequency')\n",
    "\n",
    "plt.subplot(2,2,3) # Subplots grid: 2 row, 2 columns. This is subplot 3\n",
    "plt.hist(B['brix'],10)\n",
    "plt.title('B')\n",
    "plt.xlabel('Brix')\n",
    "plt.ylabel('Frequency')\n",
    "\n",
    "plt.subplot(2,2,4) # Subplots grid: 2 row, 2 columns. This is subplot 4\n",
    "plt.hist(C['brix'],10)\n",
    "plt.title('C')\n",
    "plt.xlabel('Brix')\n",
    "plt.ylabel('Frequency')\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Exercise 2\n",
    "Look at the histograms above.\n",
    "* Which variety is the sweetest?\n",
    "* Which variety presents a larger variation in its brix value?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Challenges in the dataset\n",
    "There are a number of challenges in this dataset:\n",
    "* The number of input variables is large (p = 150)\n",
    "* The number of data samples for each variety is low (n = 201, 209, 193 respectively)\n",
    "* The data is highly collinear (close wavelenghts are very related each other).\n",
    "\n",
    "Therefore, we might want to reduce the number of wavelenghts used to predict the °Brix value. We've already created a funtion which does this to you.\n",
    "\n",
    "#### Exercise 3:\n",
    "* The code below get X and y from the dataset using the function above. Play with the variable `reduce_wavelengths` and set it to 1, 2, 3 to see what happens to the size of X when you change this value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "reduce_wavelengths = 1 # TO-DO: change (>1) value and see what happens\n",
    "\n",
    "# Get X and y from dataset using the given function\n",
    "X, y = get_X_y_data(dataset, y_name='brix', reduce_wavelengths=reduce_wavelengths)\n",
    "\n",
    "# Check that the size of the arrays are correct\n",
    "print('X shape', X.shape) # X size should be (number of entries, \n",
    "                            # (number of dataset variables-2)/reduce_wavelengths)\n",
    "print('y shape', y.shape) # y size should be (number of entries,)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Linear Regression\n",
    "\n",
    "We will now train a multiple linear regression model on the data.\n",
    "\n",
    "#### Exercise 4:\n",
    "* Go back to the notebook `Introduction to Linear Regression in python` and copy the lines of code to train a Linear Regression model\n",
    "* Put the code in the cell below \n",
    "* Go back to the notebook `Introduction to Linear Regression in python` and copy the lines of code to evaluate a Linear Regression model\n",
    "* Put the code in the cell below \n",
    "* Run the code and evaluate the performance. What is the Mean Squared Error and $R^2$ on the test set?\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Step 1: Get the data and reduce the number of wavelengths\n",
    "reduce_wavelengths=2 # TO-DO: change (>1) value and see what happens\n",
    "X, y = get_X_y_data(dataset, y_name='brix', reduce_wavelengths=reduce_wavelengths)\n",
    "\n",
    "# Step 2: Split X and y in train and test sets\n",
    "X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)\n",
    "\n",
    "# Step 3: Train a linear regression model\n",
    "# PUT YOUR CODE HERE (2 lines)\n",
    "regressor = \n",
    "\n",
    "# Step 4: Evaluate the model\n",
    "# PUT YOUR CODE HERE (2 lines)\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Overfitting\n",
    "In the previous notebook we explained the concept of overfiting in a MLP. However, this problem can as well happen in a liner regression model. Specially when the number of inputs variables is large in comparison when the amount of data. Exactly what is happening in our dataset.\n",
    "\n",
    "#### Exercise 5:\n",
    "* Run the code in the next code block to look at the performance of Linear Regression as a function of the numbner of wavelengths used\n",
    "* What is the optimal number of wavelengths to predict the Brix value?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Initialize lists to store results\n",
    "train_mse_list = []\n",
    "test_mse_list = []\n",
    "train_r2_list = []\n",
    "test_r2_list = []\n",
    "\n",
    "reductions= [i for i in range(10)]\n",
    "reductions = reductions[::-1]\n",
    "sizes = []\n",
    "\n",
    "for i in reductions:\n",
    "    X, y = get_X_y_data(dataset, y_name='brix', reduce_wavelengths=i)\n",
    "    sizes.append(X.shape[1])\n",
    "    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)\n",
    "    \n",
    "    regressor = LinearRegression()  # Create linear regression object\n",
    "    #regressor = Lasso(alpha=0.001)  # Create linear regression object\n",
    "    regressor.fit(X_train, y_train) # Training the linear regression\n",
    "\n",
    "    # Calculate performance metric with given function\n",
    "    mse_train, mse_test, r2_train, r2_test = evaluate_performace_regression(regressor, X_train, X_test, y_train, \n",
    "                                                               y_test, to_print=False)\n",
    "    # Store results in lists\n",
    "    train_mse_list.append(mse_train)\n",
    "    test_mse_list.append(mse_test)\n",
    "    train_r2_list.append(r2_train)\n",
    "    test_r2_list.append(r2_test)\n",
    "    \n",
    "    # Plot results\n",
    "    plt.subplot(1,2,1) # Subplots matrix: 1 row, 2 columns. This is subplot 1\n",
    "    plt.plot(sizes, train_mse_list, label='train')\n",
    "    plt.plot(sizes, test_mse_list, label='test')\n",
    "    plt.legend()\n",
    "    plt.xlabel('Number of wavelengths')\n",
    "    plt.ylabel('MSE [-]')\n",
    "    plt.title('MSE')\n",
    "    \n",
    "    plt.subplot(1,2,2) # Subplots matrix: 1 row, 2 columns. This is subplot 2\n",
    "    plt.plot(sizes, train_r2_list, label='train')\n",
    "    plt.plot(sizes, test_r2_list, label='test')\n",
    "    plt.legend()\n",
    "    plt.xlabel('Number of wavelengths')\n",
    "    plt.ylabel('R2 [-]')\n",
    "    plt.title('R2')\n",
    "    \n",
    "    display.clear_output(wait=True) # update plots\n",
    "    plt.show() \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Exercise 6:\n",
    "* In the next cell, set the `reduce_wavelengths` parameter to the optimal one based on what you see in the graphs above.\n",
    "* Add again the two lines of code to train the Linear Regression models\n",
    "\n",
    "Hint:\n",
    "\n",
    "| **Reduce wavelenghts factor** | **number of wavelenghts** |\n",
    "| :------------- | -----------: |\n",
    "| 1 | 150 |\n",
    "| 2 | 75 |\n",
    "| 3 | 50 |\n",
    "| 4 | 38 |\n",
    "| 5 | 30 |\n",
    "| 6 | 25 |\n",
    "| 7 | 22 |\n",
    "| 8 | 19 |\n",
    "| 9 | 17 |\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "reduce_wavelengths = 1 # TO-DO: Set to optimal\n",
    "\n",
    "# Get X and y from dataset using the given function\n",
    "X, y = get_X_y_data(dataset, y_name='brix', reduce_wavelengths=reduce_wavelengths)\n",
    "\n",
    "# Split X and y in train and test sets\n",
    "X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)\n",
    "\n",
    "# WRITE YOUR CODE HERE (2 lines)\n",
    "regressor = \n",
    "\n",
    "\n",
    "# Show the results\n",
    "evaluate_performace_regression(regressor, X_train, X_test, y_train, y_test)\n",
    "plot_regression_performance(regressor, X_test, y_test)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Neural Network\n",
    "\n",
    "Now, let's see if we can obtain better prediction of tomato sweetness by using a neural network.\n",
    "\n",
    "We first need to investigate what the optimal number of hidden layers is for this dataset.\n",
    "\n",
    "#### Exercise 7:\n",
    "* Run the code below and chose the optimal number of hidden layers."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Initialize lists to store results\n",
    "train_mse_list = []\n",
    "test_mse_list = []\n",
    "train_r2_list = []\n",
    "test_r2_list = []\n",
    "n_layers = [0, 2, 4, 6, 8] # Create list containing some possible number of hidden layers to test the model\n",
    "\n",
    "fig, a = plt.subplots(1,2)\n",
    "\n",
    "for i in n_layers: # Loop over elements in list\n",
    "    X, y = get_X_y_data(dataset, y_name='brix', reduce_wavelengths=1)\n",
    "    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)\n",
    "    \n",
    "    MLP = neural_network(i,64) # Create Neural Network\n",
    "    MLP.fit(X_train, y_train) # Train the algorithm\n",
    "    \n",
    "    # Calculate performance metric with given function\n",
    "    mse_train, mse_test, r2_train, r2_test = evaluate_performace_regression(MLP, X_train, X_test, y_train, \n",
    "                                                               y_test, to_print=False)\n",
    "    \n",
    "    # Store results in lists\n",
    "    train_mse_list.append(mse_train)\n",
    "    test_mse_list.append(mse_test)\n",
    "    train_r2_list.append(r2_train)\n",
    "    test_r2_list.append(r2_test)\n",
    "    \n",
    "    # Plot results\n",
    "    plt.subplot(1,2,1) # Subplots matrix: 1 row, 2 columns. This is subplot 1\n",
    "    plt.plot(n_layers[:len(train_mse_list)], train_mse_list, label='train')\n",
    "    plt.plot(n_layers[:len(test_mse_list)], test_mse_list, label='test')\n",
    "    plt.legend()\n",
    "    plt.xlabel('Number of wavelengths')\n",
    "    plt.ylabel('MSE [-]')\n",
    "    plt.title('MSE')\n",
    "    \n",
    "    plt.subplot(1,2,2) # Subplots matrix: 1 row, 2 columns. This is subplot 2\n",
    "    plt.plot(n_layers[:len(train_mse_list)], train_r2_list, label='train')\n",
    "    plt.plot(n_layers[:len(test_mse_list)], test_r2_list, label='test')\n",
    "    plt.legend()\n",
    "    plt.xlabel('Number of wavelengths')\n",
    "    plt.ylabel('R2 [-]')\n",
    "    plt.title('R2')\n",
    "    \n",
    "    display.clear_output(wait=True) # update plots\n",
    "    plt.show() "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that we know the optimal number of hidden layers, we can train and test the MLP.\n",
    "\n",
    "#### Exercise 8:\n",
    "* Go back to the notebook Introduction to Linear Regression in python and copy the lines of code to train a Linear Regression model\n",
    "* Put the code in the cell below\n",
    "* Go back to the notebook Introduction to Linear Regression in python and copy the lines of code to evaluate a Linear Regression model\n",
    "* Put the code in the cell below\n",
    "* Run the code and evaluate the performance. What is the Mean Squared Error and $R^2$ on the test set?\n",
    "* Are the results better than using the Linear Regression with reduced number of input variables (Exercise 6)?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Step: Train a neural network / multi-layer perceptron (MLP) \n",
    "# WRITE YOUR CODE HERE (2 lines)\n",
    "MLP = \n",
    "\n",
    "# Show the performance\n",
    "# WRITE YOUR CODE HERE (2 lines)\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
